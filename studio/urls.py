"""studio URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib import admin

from studio import settings

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^contacts/$', 'contact.views.contact_view', name='contacts'),
    url(r'^photo/category/$', 'photo.views.category_view', name='photo_category'),
    url(r'^photo/category/(?P<slug>[-_\w]+)/$', 'photo.views.photo_view', name='photo_category_items'),
    url(r'^video/(?P<slug>[-_\w]+)/$', 'video.views.video_view', name='videos'),
    url(r'^contacts/create/$', 'contact.views.contact_create'),
    url(r'^$', 'main.views.main_view', name='Home'),
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
