from django.contrib import admin
from .models import Social, Slider, FooterIcon, Category, Logo, VideoBanner, CategoryPhoto
# Register your models here.


class CategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('first_word', 'second_word')}
    list_display = ('first_word', 'slug',)


class PhotoCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('first_word', 'second_word')}
    list_display = ('first_word', 'slug',)

admin.site.register(Slider)
admin.site.register(Social)
admin.site.register(FooterIcon)
admin.site.register(Category, CategoryAdmin)
admin.site.register(Logo)
admin.site.register(VideoBanner)
admin.site.register(CategoryPhoto, PhotoCategoryAdmin)

