# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.db import models

# Create your models here.


class Logo(models.Model):
    class Meta:
        verbose_name_plural = "Наш логотип"

    logo = models.ImageField(_("Лого"), upload_to='main/logo')


class Social(models.Model):
    class Meta:
        verbose_name_plural = "Иконки соц.сетей"

    title = models.CharField(_("Название соц.сети"), max_length=30)
    icon = models.ImageField(_("Иконка соц.сети"), upload_to='main/social')
    url = models.CharField(_("Ссылка на нашу страницу в соц.сети"), max_length=100)

    def __unicode__(self):
        return self.title


class Slider(models.Model):
    class Meta:
        verbose_name_plural = "Картинки для слайдера"

    slider_image = models.ImageField(_("Картинки для слайдера"), upload_to='main/slider')
    text = models.CharField(_("Текст"), max_length=50, default=None)


class Category(models.Model):
    class Meta:
        verbose_name_plural = 'Категории на главной'

    category_image = models.ImageField(_("Изображение для категории"), upload_to='main/category')
    first_word = models.CharField(_("Первое слово"), max_length=50, default=None)
    second_word = models.CharField(_("Второе слово"), max_length=50, default=None)
    slug = models.SlugField(max_length=55, default=None)

    def __unicode__(self):
        return self.first_word + self.second_word


class CategoryPhoto(models.Model):
    class Meta:
        verbose_name_plural = "Одна категория для фото на главной (только одна)"

    category_image = models.ImageField(_("Изображение для категории"), upload_to='main/category')
    first_word = models.CharField(_("Первое слово"), max_length=50, default=None)
    second_word = models.CharField(_("Второе слово"), max_length=50, default=None)
    slug = models.SlugField(max_length=55, default=None)

    def __unicode__(self):
        return self.first_word + self.second_word


class FooterIcon(models.Model):
    class Meta:
        verbose_name_plural = "Футер"

    icon = models.ImageField(_("Иконка 4К в футере"), upload_to='main/footer')
    large_text = models.CharField(_("Большой текст рядом с лого"), max_length=100)
    small_text = models .CharField(_("Маленький текст рядом с лого"), max_length=100)
    banner_text = models.CharField(_("Самый большой текст в футере"), max_length=30, default=None)

    def __unicode__(self):
        return self.large_text


class VideoBanner(models.Model):
    class Meta:
        verbose_name_plural = "Видео - как баннер на главной"

    video = models.FileField(_("Видео"), upload_to='main/video_banner')
    is_active = models.BooleanField(_("Активно ли видео ?"), default=False, help_text="Проигрывается только одно активное видео")
