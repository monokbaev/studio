# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-29 06:47
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='footericon',
            name='banner_text',
            field=models.CharField(default=None, max_length=30, verbose_name='\u0421\u0430\u043c\u044b\u0439 \u0431\u043e\u043b\u044c\u0448\u043e\u0439 \u0442\u0435\u043a\u0441\u0442 \u0432 \u0444\u0443\u0442\u0435\u0440\u0435'),
        ),
        migrations.AlterField(
            model_name='footericon',
            name='large_text',
            field=models.CharField(max_length=100, verbose_name='\u0411\u043e\u043b\u044c\u0448\u043e\u0439 \u0442\u0435\u043a\u0441\u0442 \u0440\u044f\u0434\u043e\u043c \u0441 \u043b\u043e\u0433\u043e'),
        ),
        migrations.AlterField(
            model_name='footericon',
            name='small_text',
            field=models.CharField(max_length=100, verbose_name='\u041c\u0430\u043b\u0435\u043d\u044c\u043a\u0438\u0439 \u0442\u0435\u043a\u0441\u0442 \u0440\u044f\u0434\u043e\u043c \u0441 \u043b\u043e\u0433\u043e'),
        ),
    ]
