# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-02-29 08:21
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('main', '0004_auto_20160229_0729'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='category',
            name='category_text',
        ),
        migrations.AddField(
            model_name='category',
            name='first_word',
            field=models.CharField(default=None, max_length=50, verbose_name='\u041f\u0435\u0440\u0432\u043e\u0435 \u0441\u043b\u043e\u0432\u043e'),
        ),
        migrations.AddField(
            model_name='category',
            name='second_word',
            field=models.CharField(default=None, max_length=50, verbose_name='\u0412\u0442\u043e\u0440\u043e\u0435 \u0441\u043b\u043e\u0432\u043e'),
        ),
    ]
