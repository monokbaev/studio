from django.shortcuts import render_to_response
from .models import Slider, Social, Category, FooterIcon, Logo, VideoBanner, CategoryPhoto
# Create your views here.


def main_view(request, context={}):
    context['socials'] = Social.objects.all()
    context['sliders'] = Slider.objects.all()
    context['categories'] = Category.objects.all()
    context['photo_category'] = CategoryPhoto.objects.all()
    context['footers'] = FooterIcon.objects.all()
    context['logo'] = Logo.objects.get()
    context['video_banner'] = VideoBanner.objects.filter(is_active=True)
    return render_to_response('index.html', context)


def footer_view(request, context={}):
    context['footers'] = FooterIcon.objects.all()
    return render_to_response('footer.html', context)
