# coding=utf-8
from django.core.mail import send_mail
from django.http import HttpResponse
from django.shortcuts import render_to_response
from django.template.context_processors import csrf

from main.models import Social, FooterIcon
from photo.models import LogoGallery
from .models import ContactInfo, User, Banner


# Create your views here.

def contact_view(request, context={}):
    context.update(csrf(request))
    context['socials'] = Social.objects.all()
    context['footers'] = FooterIcon.objects.all()
    context['logos'] = LogoGallery.objects.all()
    context['banner'] = Banner.objects.get()
    context['contacts'] = ContactInfo.objects.get()
    context['users'] = User.objects.all()
    name = request.POST.get('name')
    context['the_name'] = name
    return render_to_response("contact.html", context)


def contact_create(request):
    if request.method == 'POST':
        name = request.POST.get('name')
        phone = request.POST.get('phone')
        user = User(name=name, phone=phone)
        user.save()
        name = name.encode('utf-8')
        name = 'Мое имя: ' + str(name) + '\n'
        phone = "Мой номер: " + str(phone) + "\n"
        user_id = str(user.id)
        send_mail('Заявка из сайта 4kstudio', name + phone + 'Заявка номер: ' + user_id, 'monokbaev@gmail.com', ['monokbaev@gmail.com'], fail_silently=False)
        return HttpResponse("Ваша заявка принята, мы перезвоним вам в самое ближайшее время")
