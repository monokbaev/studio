from django.contrib import admin
from .models import ContactInfo, User, Banner
# Register your models here.


class UserAdmin(admin.ModelAdmin):
    list_filter = ['id']

admin.site.register(ContactInfo)
admin.site.register(User, UserAdmin)
admin.site.register(Banner)

