# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.db import models

# Create your models here.


class User(models.Model):
    class Meta:
        verbose_name_plural = "Заявки"

    name = models.CharField(_("Имя пользователя"), max_length=50)
    phone = models.BigIntegerField(_("Номер телефона"))

    def __unicode__(self):
        return self.name


class ContactInfo(models.Model):
    class Meta:
        verbose_name_plural = "Наши контакты"

    city = models.CharField(_("Город, в котором мы находимся"), max_length=20, default=None)
    address = models.CharField(_("Наш адрес"), max_length=100)
    first_number = models.CharField(_("Первый телефон"), max_length=20)
    second_number = models.CharField(_("Второй телефон (Необязательно)"), max_length=20, blank=True, null=True)
    first_email = models.EmailField(_("Первая электронная почта"))
    second_email = models.EmailField(_("Вторая элекстронная почта (Необязательно)"), blank=True, null=True)

    def __unicode__(self):
        return self.address


class Banner(models.Model):
    class Meta:
        verbose_name_plural = "Баннер страницы Контакты"

    banner = models.ImageField(_("Фотография"), upload_to='contact/banner')
