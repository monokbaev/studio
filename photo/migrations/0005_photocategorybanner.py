# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-03 06:39
from __future__ import unicode_literals

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('photo', '0004_photo_slug'),
    ]

    operations = [
        migrations.CreateModel(
            name='PhotoCategoryBanner',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('banner', models.ImageField(upload_to='photo/banner', verbose_name='\u0421\u0430\u043c \u0431\u0430\u043d\u043d\u0435\u0440')),
                ('category', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='photo.PhotoCategory', verbose_name='\u041a \u043a\u0430\u043a\u043e\u0439 \u0424\u043e\u0442\u043e\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0438 \u043e\u0442\u043d\u043e\u0441\u0438\u0442\u0441\u044f')),
            ],
            options={
                'verbose_name_plural': '\u0411\u0430\u043d\u043d\u0435\u0440 \u0434\u043b\u044f \u043a\u0430\u0436\u0434\u043e\u0439 \u0441\u0442\u0440\u0430\u043d\u0438\u0446\u044b \u0444\u043e\u0442\u043e\u043a\u0430\u0442\u0435\u0433\u043e\u0440\u0438\u0439',
            },
        ),
    ]
