from django.contrib import admin
from .models import PhotoCategory, Photo, CategoryBanner, LogoGallery, PhotoCategoryBanner
# Register your models here.


class PhotoCategoryAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('category',)}
    list_display = ('category', 'slug',)


class PhotoAdmin(admin.ModelAdmin):
    prepopulated_fields = {'slug': ('category',)}
    list_display = ('category', 'slug',)

admin.site.register(Photo, PhotoAdmin)
admin.site.register(PhotoCategory, PhotoCategoryAdmin)
admin.site.register(CategoryBanner)
admin.site.register(LogoGallery)
admin.site.register(PhotoCategoryBanner)