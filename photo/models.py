# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.db import models

# Create your models here.


class PhotoCategory(models.Model):
    class Meta:
        verbose_name_plural = "Категории фотографий"

    category = models.CharField(_("Название категории"), max_length=50)
    slug = models.SlugField(max_length=55, default=None)
    description = models.CharField(_("Описание "), max_length=200)
    image = models.ImageField(_("Фотография категории"), upload_to='photo/category')

    def __unicode__(self):
        return self.category


class Photo(models.Model):
    class Meta:
        verbose_name_plural = "Фотографии"

    category = models.ForeignKey(PhotoCategory, verbose_name="К какой категории относится")
    slug = models.SlugField(max_length=55, default=None)
    image = models.ImageField(_("Фотография"), upload_to="photo/photo")


class CategoryBanner(models.Model):
    class Meta:
        verbose_name_plural = "Баннер на странице фотокатегорий"

    banner = models.ImageField(_("Баннер"), upload_to='photo/banner')


class PhotoCategoryBanner(models.Model):
    class Meta:
        verbose_name_plural = "Баннер для каждой страницы фотокатегорий"

    category = models.ForeignKey(PhotoCategory, verbose_name='К какой Фотокатегории относится')
    banner = models.ImageField(_("Сам баннер"), upload_to='photo/banner')


class LogoGallery(models.Model):
    class Meta:
        verbose_name_plural = "Лого на страницах галереи"

    logo = models.ImageField(_("Наш логотип"), upload_to="photo/logo")