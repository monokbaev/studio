from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
from django.shortcuts import render_to_response

from main.models import Social, FooterIcon
from .models import PhotoCategory, Photo, CategoryBanner, LogoGallery, PhotoCategoryBanner
# Create your views here.


def category_view(request, context={}):
    context['categories'] = PhotoCategory.objects.all()
    context['social_icons'] = Social.objects.all()
    context['footers'] = FooterIcon.objects.all()
    context['banner'] = CategoryBanner.objects.get()
    context['logo'] = LogoGallery.objects.get()
    return render_to_response('category.html', context)


def photo_view(request, context={}, slug=""):
    photo_list = Photo.objects.filter(category__slug=slug)
    paginator = Paginator(photo_list, 1)
    page = request.GET.get('page')

    try:
        photo = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        photo = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        photo = paginator.page(paginator.num_pages)

    context['photos'] = photo
    context['categories'] = PhotoCategory.objects.get(slug=slug)
    context['banner'] = PhotoCategoryBanner.objects.get(category__slug=slug)
    context['social_icons'] = Social.objects.all()
    context['footers'] = FooterIcon.objects.all()
    context['logo'] = LogoGallery.objects.get()
    return render_to_response('photos.html', context)
