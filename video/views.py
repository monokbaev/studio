from django.core.paginator import Paginator, PageNotAnInteger, EmptyPage
from django.shortcuts import render_to_response
# Create your views here.
from main.models import Social, FooterIcon, Category
from photo.models import LogoGallery
from .models import VideoBanner, Video


def video_view(request, context={}, slug=""):
    context['logo'] = LogoGallery.objects.get()
    context['banner'] = VideoBanner.objects.get(category__slug=slug)
    video_list = Video.objects.filter(category__slug=slug)
    paginator = Paginator(video_list, 1)
    page = request.GET.get('page')

    try:
        videos = paginator.page(page)
    except PageNotAnInteger:
        # If page is not an integer, deliver first page.
        videos = paginator.page(1)
    except EmptyPage:
        # If page is out of range (e.g. 9999), deliver last page of results.
        videos = paginator.page(paginator.num_pages)

    context['video'] = videos
    context['social_icons'] = Social.objects.all()
    context['footers'] = FooterIcon.objects.all()
    context['category'] = Category.objects.get(slug=slug)
    return render_to_response('videogallery.html', context)
