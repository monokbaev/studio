from django.contrib import admin
from .models import Video, VideoBanner
# Register your models here.

admin.site.register(Video)
admin.site.register(VideoBanner)
