# -*- coding: utf-8 -*-
# Generated by Django 1.9.2 on 2016-03-02 16:14
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('video', '0003_auto_20160302_1849'),
    ]

    operations = [
        migrations.AddField(
            model_name='video',
            name='title',
            field=models.CharField(default=None, max_length=50, verbose_name='\u041d\u0430\u0437\u0432\u0430\u043d\u0438\u0435 \u0432\u0438\u0434\u0435\u043e'),
        ),
        migrations.AlterField(
            model_name='video',
            name='poster',
            field=models.ImageField(default=None, upload_to='video/poster', verbose_name='\u041f\u043e\u0441\u0442\u0435\u0440 \u0434\u043b\u044f \u0432\u0438\u0434\u0435\u043e'),
        ),
    ]
