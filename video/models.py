# coding=utf-8
from __future__ import unicode_literals
from django.utils.translation import ugettext as _
from django.db import models
from main.models import Category

# Create your models here.


class Video(models.Model):
    class Meta:
        verbose_name_plural = "Видео"

    category = models.ForeignKey(Category, verbose_name="К какой категории относится")
    title = models.CharField(_("Название видео"), max_length=50, default=None)
    video_link = models.CharField(_("Ссылка к видео"), max_length=50)
    poster = models.ImageField(_("Постер для видео"), upload_to='video/poster', default=None)


class VideoBanner(models.Model):
    class Meta:
        verbose_name_plural = "Баннер для видео-страниц"

    category = models.ForeignKey(Category, verbose_name="К какой категории относится")
    image = models.ImageField(_("Баннер"), upload_to='video/banner')